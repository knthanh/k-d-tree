//
// Created by knthanh on 15/06/2017.
//

#include "KDBNode.h"
Node::Node(size_t dimension)
{
    _left = _right = nullptr;
}

RegionNode::RegionNode(size_t dimension, float min, float max, float mid)
: Node(dimension)
{
    _min = min;
    _max = max;
    _mid = mid;
}

float RegionNode::GetDimension()
{
    return _dimension;
}

PointNode::PointNode(size_t dimension)
: Node(dimension)
{

}

PointNode::PointNode(size_t dimension, size_t maxPoint)
: Node(dimension)
{
    _maxPoint = maxPoint;
}

PointNode::PointNode(MatrixList matrixList, size_t dimension, size_t maxPoint)
: Node(dimension), _matrixList(matrixList), _maxPoint(maxPoint)
{
}

MatrixList PointNode::GetMatrixList()
{
    return _matrixList;
}

dlib::matrix<float, 0, 1> PointNode::FindNearest(dlib::matrix<float, 0, 1> point)
{
    if(_matrixList.size() < 1) return dlib::matrix<float, 0, 1>();

    dlib::matrix<float, 0, 1> minMat = _matrixList[0];
    double minVal = MahattanDistance(point, _matrixList[0]);
    for(int i = 1; i < _matrixList.size(); ++i)
    {
        double temp = MahattanDistance(point, _matrixList[i]);
        if(minVal < temp)
        {
            minVal = temp;
            minMat = _matrixList[i];
        }
    }
    return minMat;
};