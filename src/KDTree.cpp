//
// Created by knthanh on 07/06/2017.
//

#include "KDTree.h"

KDTree::KDTree(int dimension)
{
    _dimension = dimension;
    _root = nullptr;
}

std::shared_ptr<KDNode> KDTree::Query(KDNode query)
{
    if(_root == nullptr)
        return nullptr;

    std::shared_ptr<KDNode> curNode, prevNode;
    curNode = prevNode = _root;
    int curD = 0;
    while(curNode !=nullptr)
    {
        prevNode = curNode;
        if((*curNode)[curD] < query[curD])
        {
            curNode = curNode->_right;
        }
        else
        {
            curNode = curNode->_left;
        }

        ++curD;
        if(curD >= _dimension)
        {
            curD = 0;
        }
    }

    std::shared_ptr<KDNode> nearestNode;
    nearestNode = curNode = prevNode;
    double minDis = nearestNode->ManhattanDistance(query);
    while(curNode !=nullptr)
    {
        double distance = curNode->ManhattanDistance(query);
        if(distance < minDis)
        {
            minDis = distance;
            nearestNode = curNode;
        }
        curNode = curNode->_parent;
    }
    return nearestNode;
}

std::shared_ptr<KDNode> RFindMin(std::shared_ptr<KDNode> node, int d)
{
    if(node ==nullptr)
    {
        return nullptr;
    }

    if(node->GetDimension() == d)
    {
        if(node->_left ==nullptr)
        {
            return node;
        }
        return RFindMin(node->_left, d);
    }

    std::shared_ptr<KDNode> min = node,
                            minNodeLeft = RFindMin(node->_left, d),
                            minNodeRight = RFindMin(node->_right, d);


    if(minNodeLeft !=nullptr &&
        (*minNodeLeft)[d] < (*min)[d])
    {
        min = minNodeLeft;
    }

    if(minNodeRight !=nullptr &&
        (*minNodeRight)[d] < (*min)[d])
    {
        min = minNodeRight;
    }

    return min;
}

std::shared_ptr<KDNode> KDTree::FindMinAt(int dimension)
{
    if(_root ==nullptr)
    {
        return nullptr;
    }

    return RFindMin(_root, dimension);
}

float KDTree::FindMinValAt(int dimension)
{
    return (*FindMinAt(dimension))[dimension];
}

std::shared_ptr<KDNode> RFindMax(std::shared_ptr<KDNode> node, int d)
{
    if(node ==nullptr)
    {
        return nullptr;
    }

    if(node->GetDimension() == d)
    {
        if(node->_right ==nullptr)
        {
            return node;
        }
        return RFindMax(node->_right, d);
    }

    std::shared_ptr<KDNode> max = node,
        maxNodeLeft = RFindMax(node->_left, d),
        maxNodeRight = RFindMax(node->_right, d);


    if(maxNodeLeft !=nullptr &&
        (*maxNodeLeft)[d] > (*max)[d])
    {
        max = maxNodeLeft;
    }

    if(maxNodeRight !=nullptr &&
        (*maxNodeRight)[d] > (*max)[d])
    {
        max = maxNodeRight;
    }

    return max;
}

std::shared_ptr<KDNode> KDTree::FindMaxAt(int dimension)
{
    if(_root ==nullptr)
    {
        return nullptr;
    }

    return RFindMax(_root, dimension);
}

float KDTree::FindMaxValAt(int dimension)
{
    return (*FindMaxAt(dimension))[dimension];
}

void KDTree::Insert(KDNode newNode)
{
    if(_root == nullptr)
    {
        _root = std::make_shared<KDNode>(newNode.GetVal());
        return;
    }

    std::shared_ptr<KDNode> curNode, prevNode;
    curNode = prevNode = _root;
    int curDimension = 0;
    while(curNode != nullptr)
    {
        prevNode = curNode;
        if((*curNode)[curDimension] > newNode[curDimension])
        {
            curNode = curNode->_left;
        }
        else
        {
            curNode = curNode->_right;
        }

        ++curDimension;
        if(curDimension >= _dimension)
        {
            curDimension = 0;
        }
    }

    if((*prevNode)[curDimension - 1] > newNode[curDimension - 1])
    {
        prevNode->_left = std::make_shared<KDNode>(newNode);
        prevNode->_left->_parent = prevNode;
    }
    else
    {
        prevNode->_right = std::make_shared<KDNode>(newNode);
        prevNode->_right->_parent = prevNode;
    }
}

void KDTree::Delete(KDNode node)
{
    // TODO
}