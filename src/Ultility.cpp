//
// Created by knthanh on 14/06/2017.
//

#include "Ultility.h"

int QuickSortPartition(MatrixList &matrixList, int d, int bot, int top)
{
    float pivot = matrixList[bot](d,0);

    int i = bot, j = top;
    while(true)
    {
        while(matrixList[i](d,0) < pivot) ++i;
        while(matrixList[j](d,0) > pivot) --j;

        if(i >= j) return j;

        std::iter_swap(matrixList.begin() + i, matrixList.begin() + j);
        ++i;--j;
    }
}

void RQuickSort(MatrixList& matrixList, int d, int bot, int top)
{
    if(bot >= top) return;

    if(matrixList.size() <= 1)
    {
        return;
    }

    int pivot = QuickSortPartition(matrixList, d, bot, top);
    RQuickSort(matrixList, d, bot, pivot);
    RQuickSort(matrixList, d, pivot + 1, top);
}

void QuickSort(MatrixList& matrixList, int d)
{
    RQuickSort(matrixList, d, 0, matrixList.size() - 1);
}

double MahattanDistance(dlib::matrix<float,0,1> A, dlib::matrix<float,0,1> B)
{
    auto C = A - B;
    double distance = 0;
    for(auto it = C.begin(); it != C.end(); ++it)
    {
        distance += std::abs(*it);
    }
    return distance;
}

double EulerDistance(dlib::matrix<float,0,1> A, dlib::matrix<float,0,1> B)
{
    auto C = A - B;
    double distance = 0;
    for(auto it = C.begin(); it != C.end(); ++it)
    {
        distance += (*it) * (*it);
    }
    return std::sqrt(distance);
}