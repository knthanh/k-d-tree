//
// Created by knthanh on 15/06/2017.
//

#include "KDBTree.h"

KDBTree::KDBTree(size_t dimension, size_t maxPoint)
{
    _dimension = dimension;
    _maxPoint = maxPoint;
    _size = 0;
    _root = nullptr;
}

KDBNodePtr KDBTree::Split(KDBNodePtr node)
{

}

void KDBTree::Insert(dlib::matrix<float, 0, 1> point)
{

}

void KDBTree::Delete(dlib::matrix<float, 0, 1> point)
{

}

bool KDBTree::IsContainLeft(std::shared_ptr<RegionNode> node, dlib::matrix<float, 0, 1> point)
{
    if(node == nullptr) return false;

    return node->_min <= point(node->_dimension) &&
    point(node->_dimension) < node->_mid;
}

dlib::matrix<float, 0, 1> KDBTree::FindNearest(dlib::matrix<float, 0, 1> point)
{
    if(_root == nullptr) return dlib::matrix<float, 0, 1>();

    KDBNodePtr curNode;

    curNode = _root;
    std::shared_ptr<PointNode> pointPage;

    while(curNode != nullptr)
    {
        if((pointPage = std::dynamic_pointer_cast<PointNode>(curNode)) != nullptr)
        {
            return pointPage->FindNearest(point);
        }
        else
        {
            if(IsContainLeft(std::dynamic_pointer_cast<RegionNode>(curNode), point))
            {
                curNode = curNode->_left;
            }
            else
            {
                curNode = curNode->_right;
            }
        }
    }

    return dlib::matrix<float, 0, 1>();
}

KDBNodePtr KDBTree::RBuildFrom(MatrixList matrixList, int depth)
{
    ++_size;
    if (matrixList.size() <= _maxPoint)
    {
        return std::make_shared<PointNode>(matrixList, _dimension, _maxPoint);
    }

    int curD = depth % (int) _dimension;

    QuickSort(matrixList, curD);

    float avg = 0;
    for(auto& mat : matrixList)
    {
        avg += mat(curD);
    }
    avg /= matrixList.size();

    float min = matrixList.front()(curD),
        mid = avg,
        max = matrixList.back()(curD);

    std::shared_ptr<RegionNode> regionPage = std::make_shared<RegionNode>(curD, min, max, mid);


    MatrixList leftList, rightList;

    for(auto& mat : matrixList)
    {
        if(mat(curD) < avg)
        {
            leftList.push_back(mat);
        }
        else
        {
            rightList.push_back(mat);
        }
    }

    regionPage->_left = RBuildFrom(leftList, depth + 1);
    regionPage->_right = RBuildFrom(rightList, depth + 1);

    return std::make_shared<RegionNode>(*regionPage);
}

void KDBTree::BuildFrom(MatrixList matrixList)
{
    _root = RBuildFrom(matrixList, 0);
}