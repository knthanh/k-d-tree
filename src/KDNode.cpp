//
// Created by knthanh on 09/06/2017.
//

#include "KDNode.h"

KDNode::KDNode(dlib::matrix<float,0,1> value)
{
    _val = value;
    _dimension = (size_t) _val.nr();
    _left = _right = _parent = nullptr;
}
KDNode::KDNode(size_t dimension)
{
    _dimension = dimension;
    _val.set_size(_dimension);
    _left = _right = nullptr;
}

double KDNode::ManhattanDistance(KDNode node)
{
    auto resultMat = _val - node._val;
    double distance = 0;
    for(auto it = resultMat.begin(); it != resultMat.end(); ++it)
    {
        distance += std::abs(*it);
    }

    return distance;
}

double KDNode::EulerDistance(KDNode node)
{
    auto resultMat = _val - node._val;
    double distance = 0;
    for(auto it = resultMat.begin(); it != resultMat.end(); ++it)
    {
        distance += (*it) * (*it);
    }

    return std::sqrt(distance);
}

void KDNode::SetDimension(size_t dimension)
{
    _val.set_size(dimension, 1);
    _dimension = dimension;
}

int KDNode::GetDimension()
{
    return (int) _dimension;
}

void KDNode::SetVal(dlib::matrix<float,0,1> val)
{
    _val = val;
    _dimension = (size_t) _val.nr();
}

dlib::matrix<float,0,1> KDNode::GetVal()
{
    return _val;
};

float& KDNode::operator[](const int index)
{
    return _val(index, 0);
}

bool operator==(KDNode& lhs, KDNode& rhs)
{
    auto resultMat = lhs.GetVal() - rhs.GetVal();
    for(auto it = resultMat.begin(); it != resultMat.end(); ++it)
    {
        if(*it != 0)
        {
            return false;
        }
    }
    return true;
}