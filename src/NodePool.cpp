//
// Created by knthanh on 14/06/2017.
//

#include "NodePool.h"


NodePool::NodePool(size_t size)
: NodePool(size, 0)
{
}

NodePool::NodePool(size_t size, size_t dimension)
{
    _nodePool.resize(size);
    for(auto &node : _nodePool)
    {
        node = std::make_shared<KDNode>(dimension);
    }
}

NodePtr NodePool::GetNewNode()
{
    auto& node = _nodePool.back();
    _usedNodes.push_back(node);
    _nodePool.pop_back();
    return node;
}

void NodePool::ReturnNode(NodePtr node)
{
    if(std::find(_usedNodes.begin(), _usedNodes.end(), node) == _usedNodes.end()) return;

    _nodePool.push_back(node);
    _usedNodes.erase(std::remove(_usedNodes.begin(), _usedNodes.end(), node), _usedNodes.end());
}