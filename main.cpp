#include <iostream>
#include "KDBTree.h"

int main()
{
    dlib::matrix<float, 2, 1> x1,x2,x3,x4,x5,x6,x7,q1;

    x1 = 3,6;
    x2 = 17,15;
    x3 = 13,15;
    x4 = 6,12;
    x5 = 9,1;
    x6 = 2,7;
    x7 = 10,19;
    q1 = 5,11;

    MatrixList list;

    list.push_back(x1);
    list.push_back(x2);
    list.push_back(x3);
    list.push_back(x4);
    list.push_back(x5);
    list.push_back(x6);
    list.push_back(x7);

    KDBTree kdTree(2, 2);

    kdTree.BuildFrom(list);

    std::cout << kdTree.FindNearest(q1);
    return 0;
}