//
// Created by knthanh on 14/06/2017.
//

#ifndef KD_TREE_NODEPOOL_H
#define KD_TREE_NODEPOOL_H

#include <vector>
#include <string.h>
#include <memory>
#include "KDNode.h"

typedef std::shared_ptr<KDNode> NodePtr;

class NodePool
{
private:
  std::vector<NodePtr> _nodePool;
  std::vector<NodePtr> _usedNodes;

public:
  NodePool(size_t size);
  NodePool(size_t size, size_t dimension);

  NodePtr GetNewNode();
  void ReturnNode(NodePtr node);
};

#endif //KD_TREE_NODEPOOL_H
