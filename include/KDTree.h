//
// Created by knthanh on 07/06/2017.
//

#ifndef BKD_TREE_KDTREE_H
#define BKD_TREE_KDTREE_H

#include "KDNode.h"

class KDTree
{
private:
  int _dimension;
  std::shared_ptr<KDNode> _root;

public:
  KDTree(int dimension);

  std::shared_ptr<KDNode> Query(KDNode query);
  std::shared_ptr<KDNode> FindMinAt(int dimension);
  float FindMinValAt(int dimension);
  std::shared_ptr<KDNode> FindMaxAt(int dimension);
  float FindMaxValAt(int dimension);
  void Insert(KDNode newNode);
  void Delete(KDNode node);
};

#endif //BKD_TREE_KDTREE_H
