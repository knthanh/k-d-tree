//
// Created by knthanh on 09/06/2017.
//

#ifndef BKD_TREE_KDNODE_H
#define BKD_TREE_KDNODE_H

#include <dlib/matrix.h>
#include <memory>

class KDNode
{
private:
  size_t _dimension;
  dlib::matrix<float,0,1> _val;

public:
  std::shared_ptr<KDNode> _left, _right, _parent;

public:
  KDNode(dlib::matrix<float,0,1> value);
  KDNode(size_t dimension);

  double ManhattanDistance(KDNode node);
  double EulerDistance(KDNode node);

  void SetDimension(size_t dimension);
  int GetDimension();

  void SetVal(dlib::matrix<float,0,1> val);
  dlib::matrix<float,0,1> GetVal();

  float& operator[](const int index);
};
bool operator==(KDNode& lhs, KDNode& rhs);

#endif //BKD_TREE_KDNODE_H
