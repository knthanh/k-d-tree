//
// Created by knthanh on 14/06/2017.
//

#ifndef KD_TREE_ULTILITY_H
#define KD_TREE_ULTILITY_H

#include <vector>
#include <dlib/matrix.h>

typedef std::vector<dlib::matrix<float,0,1>> MatrixList;

int QuickSortPartition(MatrixList &matrixList, int d, int bot, int top);
void RQuickSort(MatrixList& matrixList, int d, int bot, int top);
void QuickSort(MatrixList& matrixList, int d);

double MahattanDistance(dlib::matrix<float,0,1> A, dlib::matrix<float,0,1> B);
double EulerDistance(dlib::matrix<float,0,1> A, dlib::matrix<float,0,1> B);

#endif //KD_TREE_ULTILITY_H
