//
// Created by knthanh on 15/06/2017.
//

#ifndef KD_TREE_KDBNODE_H
#define KD_TREE_KDBNODE_H

#include <dlib/matrix.h>
#include "Ultility.h"


class Node
{
protected:
  size_t _dimension;

public:
  Node(size_t dimension);
  virtual ~Node(){};

  std::shared_ptr<Node> _left, _right;
};

class RegionNode : public Node
{
  friend class KDBTree;
private:
  size_t _dimension;
  float  _min, _max, _mid;

public:
  RegionNode(size_t dimension, float min, float max, float mid);

  float GetDimension();
};

class PointNode : public Node
{
  friend class KDBTree;
private:
  MatrixList _matrixList;
  size_t _maxPoint;

public:
  PointNode(size_t dimension);
  PointNode(size_t dimension, size_t maxPoint = 2);
  PointNode(MatrixList matrixList, size_t dimension, size_t maxPoint = 2);

  MatrixList GetMatrixList();
  dlib::matrix<float, 0, 1> FindNearest(dlib::matrix<float, 0, 1> point);
};

typedef std::shared_ptr<Node> KDBNodePtr;

#endif //KD_TREE_KDBNODE_H
