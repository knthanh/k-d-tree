//
// Created by knthanh on 15/06/2017.
//

#ifndef KD_TREE_KDBTREE_H
#define KD_TREE_KDBTREE_H

#include "KDBNode.h"
#include "NodePool.h"
#include <memory>

class KDBTree
{
private:
  size_t _dimension, _maxPoint, _size;
  KDBNodePtr _root;

private:
  KDBNodePtr Split(KDBNodePtr node);
  bool IsContainLeft(std::shared_ptr<RegionNode> node, dlib::matrix<float, 0, 1> point);
  KDBNodePtr RBuildFrom(MatrixList matrixList, int depth);

public:
  KDBTree(size_t dimension, size_t maxPoint);

  void Insert(dlib::matrix<float, 0, 1> point);
  void Delete(dlib::matrix<float, 0, 1> point);
  dlib::matrix<float, 0, 1> FindNearest(dlib::matrix<float, 0, 1> point);

  void BuildFrom(MatrixList matrixList);
};

#endif //KD_TREE_KDBTREE_H
